// CS 61C Fall 2015 Project 4

// include SSE intrinsics
#if defined(_MSC_VER)
#include <intrin.h>
#elif defined(__GNUC__) && (defined(__x86_64__) || defined(__i386__))
#include <x86intrin.h>
#endif

// include OpenMP
#if !defined(_MSC_VER)
#include <pthread.h>
#endif
#include <omp.h>

//#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <math.h>

#include "calcDepthOptimized.h"
#include "calcDepthNaive.h"

#define MAX(a, b) ((a>b)? a:b)
#define MIN(a, b) ((a<b)? a:b)

/* DO NOT CHANGE ANYTHING ABOVE THIS LINE. */

void calcDepthOptimized(float *depth, float *left, float *right, int imageWidth, int imageHeight, int featureWidth, int featureHeight, int maximumDisplacement)
{
	// for (int i = 0; i < imageWidth * imageHeight; i++)
	// {
	// 	depth[i] = 0;
	// }

	int unrollBound8 = (2 * featureWidth + 1)/8 * 8;
	int unrollBound4 = (2 * featureWidth + 1)/4 * 4;
	int unrollBound1 = 2 * featureWidth + 1;

	__m128 r0 = _mm_setzero_ps(); 

	int totalArea = imageWidth * imageHeight;
	int depthIndex = 0;
	for (; depthIndex < totalArea / 16 * 16; depthIndex += 16)
    {
    	_mm_storeu_ps(depth + depthIndex, r0);
    	_mm_storeu_ps(depth + depthIndex + 4, r0);
    	_mm_storeu_ps(depth + depthIndex + 8, r0);
    	_mm_storeu_ps(depth + depthIndex + 12, r0);
    }

    for (; depthIndex < totalArea; depthIndex++)
	{
		depth[depthIndex] = 0;
	}

	#pragma omp parallel for
	/* The two outer for loops iterate through each pixel */

	for (int y = featureHeight; y < imageHeight - featureHeight; y++)
	{
		for (int x = featureWidth; x < imageWidth - featureWidth; x++)
		{	
			// /* Set the depth to 0 if looking at edge of the image where a feature box cannot fit. */
			// if ((y < featureHeight) || (y >= imageHeight - featureHeight) || (x < featureWidth) || (x >= imageWidth - featureWidth))
			// {
			// 	depth[y * imageWidth + x] = 0;
			// 	continue;
			// }

			float minimumSquaredDifference = -1;
			int minimumDy = 0;
			int minimumDx = 0;

			int dyStart = MAX(featureHeight - y, -1 * maximumDisplacement);
			int dxStart = MAX(featureWidth - x, -1 * maximumDisplacement);
			int dyBound = MIN(imageHeight - featureHeight - y - 1, maximumDisplacement);
			int dxBound = MIN(imageWidth - featureWidth - x - 1, maximumDisplacement);

			/* Iterate through all feature boxes that fit inside the maximum displacement box. 
			   centered around the current pixel. */
			for (int dy = dyStart; dy <= dyBound; dy++)
			{
				for (int dx = dxStart; dx <= dxBound; dx++)
				{

			// for (int dy = -maximumDisplacement; dy <= maximumDisplacement; dy++)
			// {
				// for (int dx = -maximumDisplacement; dx <= maximumDisplacement; dx++)
				// {
					/* Skip feature boxes that dont fit in the displacement box. */
					// if (y + dy - featureHeight < 0 || y + dy + featureHeight >= imageHeight || x + dx - featureWidth < 0 || x + dx + featureWidth >= imageWidth)
					// {
					// 	continue;
					// }

					float squaredDifference = 0;
					float S[4];
					__m128 sum = _mm_setzero_ps();

					//=================
							for (int boxX = 0; boxX < unrollBound8; boxX+=8)
							{
								for (int boxY = -featureHeight; boxY <= featureHeight; boxY++)
								{
									int leftIndex = (y + boxY) * imageWidth + (x + boxX - featureWidth); 
									int rightIndex = (y + dy + boxY) * imageWidth + (x + dx + boxX - featureWidth); 
					
									__m128 v1 = _mm_loadu_ps(leftIndex + left); 
									__m128 v2 = _mm_loadu_ps(rightIndex + right); 
									v1 = _mm_sub_ps(v1, v2);
									v1 = _mm_mul_ps(v1, v1);
									sum = _mm_add_ps(v1, sum);

									v1 = _mm_loadu_ps(leftIndex + 4  + left); 
									v2 = _mm_loadu_ps(rightIndex + 4 + right); 
									v1 = _mm_sub_ps(v1, v2);
									v1 = _mm_mul_ps(v1, v1);
									sum = _mm_add_ps(v1, sum);
								}
							}
							
							for (int boxX = unrollBound8; boxX < unrollBound4; boxX+=4)
							{
								for (int boxY = -featureHeight; boxY <= featureHeight; boxY++)
								{ 
									__m128 v1 = _mm_loadu_ps(left + (y + boxY) * imageWidth + (x + boxX - featureWidth)); 
									__m128 v2 = _mm_loadu_ps(right + (y + dy + boxY) * imageWidth + (x + dx + boxX - featureWidth)); 
									v1 = _mm_sub_ps(v1, v2);
									v1 = _mm_mul_ps(v1, v1);
									sum = _mm_add_ps(v1, sum);
								}
							}

							for (int boxX = unrollBound4; boxX < unrollBound1; boxX++)
							{
								for (int boxY = -featureHeight; boxY <= featureHeight; boxY++)
								{
									int leftX = x + boxX - featureWidth;
									int leftY = y + boxY;
									int rightX = leftX + dx;
									int rightY = leftY + dy;

									float difference = left[leftY * imageWidth + leftX] - right[rightY * imageWidth + rightX];
									squaredDifference += difference * difference;
								}
							}
					//=================

					_mm_store_ps(S, sum);
					squaredDifference += S[0] + S[1] + S[2] +S[3];

					/* 
					Check if you need to update minimum square difference. 
					This is when either it has not been set yet, the current
					squared displacement is equal to the min and but the new
					displacement is less, or the current squared difference
					is less than the min square difference.
					*/
					if ((minimumSquaredDifference == -1) || (minimumSquaredDifference > squaredDifference) || ((minimumSquaredDifference == squaredDifference) && (displacementNaive(dx, dy) < displacementNaive(minimumDx, minimumDy))))
					{
						minimumSquaredDifference = squaredDifference;
						minimumDx = dx;
						minimumDy = dy;
					}
				}
			}

			/* 
			Set the value in the depth map. 
			If max displacement is equal to 0, the depth value is just 0.
			*/
			if (minimumSquaredDifference != -1 && (maximumDisplacement != 0))
			{
				// if (maximumDisplacement == 0)
				// {
				// 	depth[y * imageWidth + x] = 0;
				// }
				// else
				// {
				depth[y * imageWidth + x] = displacementNaive(minimumDx, minimumDy);
			// 	}
			// }
			// else
			// {
			// 	depth[y * imageWidth + x] = 0;
			}
		}
	}
}
